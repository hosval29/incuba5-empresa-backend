const Validator = require('validator')
const isEmpty = require('is-empty')

module.exports = validateInputs = (codeSetting) => {
    let errors = {}

    codeSetting = !isEmpty(codeSetting) ? codeSetting : ""

    if (Validator.isEmpty(codeSetting)) {
        errors.codeSetting = "Campo Código vacio."
    } else if (!Validator.isLength(codeSetting, { min: 6, max: 6 })) {
        errors.codeSetting = "El número de caracteres no es permitido (3-255)."
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}