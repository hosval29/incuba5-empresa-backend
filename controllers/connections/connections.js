const Connections = require('../../models').Connections
const Clients = require('../../models').Clients

const validateInputs = require('./validator')

module.exports = {

    getDataClientByCodeSetting(req, res) {
        //console.log("OUTPUT: getDataClientByCodeSetting -> req", req.headers.codesetting)
        const _codeSetting = req.headers.codesetting

        const { errors, isValid } = validateInputs(_codeSetting)

        if (!isValid) {
            return res.status(400).json(errors)
        }

        return Connections.findOne({
            where: {
                code: _codeSetting
            }
        }).then((result) => {
            if (!result || JSON.stringify(result) == '{}') {
                return res.status(404).json({
                    codeStatus: 404,
                    message: 'Código no habilitado.'
                })
            }

            return Clients.findOne({
                include: [{
                    model: Connections, as: 'connections', where: { code: _codeSetting }
                }]
            }).then((result) => {
                if (!result || JSON.stringify(result) == '{}') {
                    return res.status(404).json({
                        codeStatus: 404,
                        message: 'Código no habilitado.'
                    })
                }

                return res.status(200).json({
                    codeStatus: 200,
                    message: 'Datos Cliente cargados con éxito.',
                    data: result
                })

            }).catch((err) => res.status(err.status || 500).json({
                message: 'Error en la consulta Obtener Código.',
                codeStatus: 500,
            }));

        }).catch((err) => res.status(err.status || 500).json({
            message: 'Error en la consulta Obtener Código.',
            codeStatus: 500,
        }));
    }
}