const clients = require('./clients/clients');
const connections = require('./connections/connections')
module.exports = {
    clients, connections
}
