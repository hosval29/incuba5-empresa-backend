const Validator = require('validator')
const isEmpty = require('is-empty')

module.exports = validateInputs = (data) => {

    let errors = {}

    data.client = !isEmpty(data.client) ? data.client : ""
    data.nit = !isEmpty(data.nit) ? data.nit : ""
    data.numberPhone = !isEmpty(data.numberPhone) ? data.numberPhone : ""
    data.address = !isEmpty(data.address) ? data.address : ""
    data.email = !isEmpty(data.email) ? data.email : ""

    if (Validator.isEmpty(data.client)) {
        errors.client = "Campo Cliente obligatorio."
    } else if (!Validator.isLength(data.client, { min: 3, max: 255 })) {
        errors.client = "El número de caracteres no es permitido (3-255)."
    }

    if (Validator.isEmpty(data.nit)) {
        errors.nit = "Campo Nit obligatorio."
    } else if (!Validator.isLength(data.nit, { min: 3, max: 14 })) {
        errors.nit = "El número de caracteres no es permitido (3-14)."
    }else if(!Validator.isNumeric(data.nit)){
        errors.nit = "Caracteres invalidos, (Solo números)."
    }

    if (Validator.isEmpty(data.numberPhone)) {
        errors.numberPhone = "Campo Número Teléfono obligatorio."
    } else if (!Validator.isLength(data.numberPhone, { min: 7, max: 7 })) {
        errors.numberPhone = "El número de caracteres no es permitido, (min: 7, max: 7)."
    }else if(!Validator.isNumeric(data.numberPhone)){
        errors.numberPhone = "Caracteres invalidos, (Solo números)."
    }

    if (Validator.isEmpty(data.address)) {
        errors.address = "Campo Dirreción obligatorio."
    } else if (!Validator.isLength(data.address, { min: 3, max: 255 })) {
        errors.address = "El número de caracteres no es permitido, (min: 3, max: 255)."
    }

    if (Validator.isEmpty(data.email)) {
        errors.email = "Campo Correo obligatorio."
    } else if (!Validator.isLength(data.email, { min: 3, max: 255 })) {
        errors.email = "El número de caracteres no es permitido, (min: 3, max: 255)."
    }else if(!Validator.isEmail(data.email)){
        errors.email = "Formato Correo Invalido."
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}