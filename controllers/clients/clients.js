const Clients = require('../../models').Clients;
const Connections = require('../../models').Connections

const RESPONSE_TAG = "Cliente"
const RESPONSES_TAG = "Clientes"

const modelConfig = require('../../models').Sequelize;
const Op = modelConfig.Sequelize.Op;

const validateInputs = require('./validator')

module.exports = {
    getAll(req, res) {
        return Clients.findAll({
            include: [{ model: Connections, as: 'connections' }]
        }).then(result => {
            if (!result || result.length != 0) {
                return res.status(404).json({ message: 'No existen registros de Clientes.' })
            }

            return res.status(200).json({
                data: result,
                message: 'Clientes cargados con éxito.'
            })

        }).catch((err) => res.status(err.status || 500).json({
            message: 'Error en la consulta Obtener Clientes.',
            error: err,
            errormessage: err.message
        }))
    },

    getById(req, res) {
        const _id = req.params.id

        return Clients.findOne({
            where: { id: _id },
            include: [
                { model: Connections, as: 'connections' }
            ]
        }).then((result) => {

            if (!result || JSON.stringify(result) == '{}') {
                return res.status(404).json({
                    message: 'Este Cliente no existe.'
                })
            }

            return res.status(200).json({
                data: result
            })

        }).catch((err) => res.status(err.status || 500).json({
            message: err.message,
            error: err,
            errormessage: err.message
        }))

    },

    add(req, res) {

        const dataAdd = {
            client: req.body.client,
            nit: req.body.nit,
            numberPhone: req.body.numberPhone,
            email: req.body.email,
            address: req.body.address,
            status: req.body.status,
            connections: req.body.connections
        }

        const { errors, isValid } = validateInputs(dataAdd)

        if (!isValid) {
            return res.status(400).json(errors)
        }

        const code = this.generateCode(1, 999999)
        //console.log("OUTPUT: add -> code", code)

        //console.log("dataAdd", dataAdd)
        return Clients.findOne({ where: { nit: dataAdd.nit } })
            .then((result) => {
                if (result) {
                    return res.status(400).json({
                        message: 'Este Nit ya se encuentra registrado.'
                    })
                }

                return Clients.create({
                    //where: { nit: dataAdd.nit },
                    //defaults: {
                    client: req.body.client,
                    nit: req.body.nit,
                    numberPhone: req.body.numberPhone,
                    address: req.body.address,
                    email: req.body.email,
                    status: req.body.status,
                    connections: req.body.connections
                    //},

                }, { include: [{ model: Connections, as: 'connections' }] })

                    .then((result) => {

                        if (!result) {
                            return res.status(400).json({
                                state: 'Erro al crear el Cliente.'
                            })
                        }

                        return res.status(201).json({
                            data: result,
                            message: 'Cliente creado con éxito.'
                        })

                    }).catch((err) => res.status(err.status || 500).json({
                        message: err.message,
                        error: err,
                        errormessage: err.message
                    }))
            }).catch((err) => {

            });


    },

    update(req, res) {

        const dataUpdate = {
            client: req.body.client,
            nit: req.body.nit,
            numberPhone: req.body.numberPhone,
            email: req.body.email,
            address: req.body.address,
            status: req.body.status
        }

        const _id = req.params.id

        const { errors, isValid } = validateInputs(dataUpdate)

        if (!isValid) {
            return res.status(400).json(errors)
        }

        return Clients.findOne({
            where: { id: _id }
        }).then((result) => {
            if (!result || JSON.stringify(result) == '{}') {
                return res.status(404).json({
                    message: 'Este Cliente no existe.'
                })
            }

            return Clients.findOne({
                where: {
                    nit: dataUpdate.nit
                }
            }).then((result) => {

                if (result) {
                    if (result.id != _id) {
                        return res.status(400).json({
                            state: 'Este Nit ya se encuentra registrado.'
                        })
                    }
                }

                return Clients.update({ ...dataUpdate },
                    {
                        where: {
                            id: _id
                        },
                        returning: true
                    }).then(([numerberAffectedRows, affectedRows]) => {
                        if (numerberAffectedRows === 0) {
                            return res.status(500).json({
                                message: `No se pudo actualizar el ${RESPONSE_TAG}`
                            })
                        }

                        return res.status(200).json({
                            data: affectedRows,
                            message: `${RESPONSE_TAG} actualizado con éxito.`
                        })

                    }).catch((err) => res.status(err.status || 500).json({
                        errormessage: err.message,
                        error: err,
                        message: 'Error en la consulta Actualizar Cliente.'
                    }))

            }).catch((err) => res.status(err.status || 500).json({
                message: 'Error en la consulta Obtener Cliente por Nit.',
                error: err,
                errormessage: err.message
            }))

        }).catch((err) => res.status(err.status || 500).json({
            message: 'Error en la consulta Obtener Cliente por ID.',
            error: err,
            errormessage: err.message
        }))
    },

    enableOrDisable(req, res) {

        const _id = req.params.id

        const statusClient = req.body.status

        return Clients.findOne({ where: { id: _id } })
            .then((result) => {
                if (!result || JSON.stringify(result)) {
                    return res.status(404).json({
                        message: `Este ${RESPONSE_TAG} no existe.`
                    })
                }

                return Clients.update({ status: statusClient },
                    {
                        where: { id: _id },
                        returning: true
                    }).then(([numerberAffectedRows, affectedRows]) => {
                        if (numerberAffectedRows === 0) {
                            return res.status(500).json({
                                message: `No se pudo actualizar el ${RESPONSE_TAG}`
                            })
                        }

                        let message = ''
                        if (statusStateVehicle == 1)
                            message = `${RESPONSE_TAG} habilitado con exito.`
                        else
                            message = `${RESPONSE_TAG} inhabilitado con exito.`

                        res.status(200).json({
                            estado: affectedRows,
                            message: message
                        })

                    }).catch((error) => res.status(error.status || 500).json({
                        message: 'Error en la consulta Actualizar Cliente.',
                        error: error,
                        errormessage: error.message
                    }))

            }).catch((err) => res.status(err.status || 500).json({
                message: 'Error en la consulta Obtener por ID.',
                errormessage: err.message,
                error: err
            }));
    }
}

generateCode = (limitLower, limitUpper) => {
    var numberPossibilities = limitUpper - limitLower
    var code = Math.random() * numberPossibilities
    code = Math.round(code)
    return parseInt(limitUpper) + code
}