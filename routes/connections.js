const express = require('express');
const router = express.Router();

const connectionsController = require('../controllers').connections;

/* getDataCoseSetting by Code Setting of Client */
router.get('/', connectionsController.getDataClientByCodeSetting);

/* getDataCoseSetting by Code Setting of Client */
router.post('/', connectionsController.getDataClientByCodeSetting);

module.exports = router;