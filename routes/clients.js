const express = require('express');
const router = express.Router();

const clientsController = require('../controllers').clients;

/* getAll items */
router.get('/', clientsController.getAll);

/* getById item */
router.get('/:id', clientsController.getById);

/* getCodeSetting item */
router.get('/:id', clientsController.getById);

/* add item */
router.post('/', clientsController.add);

/* update one attribute */
router.put('/:id', clientsController.update);

/* update => desactiva/Activa status */
router.delete('/:id', clientsController.enableOrDisable);

module.exports = router;