'use strict';
module.exports = (sequelize, DataTypes) => {
  const Connections = sequelize.define('Connections', {
    code: DataTypes.BIGINT,
    url: DataTypes.TEXT,
    urlLogo: DataTypes.TEXT,
    idClient: DataTypes.INTEGER
  }, {});
  Connections.associate = function(models) {
    // associations can be defined here
  };
  return Connections;
};