'use strict';
module.exports = (sequelize, DataTypes) => {
  const Clients = sequelize.define('Clients', {
    client: DataTypes.STRING,
    nit: DataTypes.BIGINT,
    numberPhone: DataTypes.BIGINT,
    address: DataTypes.TEXT,
    email: DataTypes.TEXT,
    status: DataTypes.INTEGER
  }, {});
  Clients.associate = function (models) {
    // associations can be defined here
    Clients.hasOne(models.Connections, {
      foreignKey: 'idClient',
      as: 'connections'
    });
  };
  return Clients;
};